# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create!(name: "Example User",
	email: "examhhyuhdfnefhmple@railstutorial.com",
	password: "foobar",
	password_confirmation: "foobar")

99.times do |n|
  name = Faker::Name.name
  email = "misi-#{n+1}@railsapp.io"
  password = "qwerty"
  
  User.create!(name: name,
  	email: email,
  	password: password,
  	password_confirmation: password)
end

users = User.order(:created_at).take(6)

50.times do
  content = Faker::Lorem.sentence(5)
  users.each do |user|
    user.micropost.create!(content: content)
  end
end





