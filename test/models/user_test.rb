require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  def setup
  	@user = User.new(name: "nfnenfv", email: "tr@fr.com", password: "qwerty", password_confirmation: "qwerty")
  end

  test "should be valid" do
  	assert @user.valid?
  end

  test "name should be present" do
    @user.name = "     "
    assert_not @user.valid?
  end

  test "email should be valid" do
  	@user.email = "   "
  	assert_not @user.valid?
  end

  test "name should not be too long" do
    @user.name = "a"*51
    assert_not @user.valid?
  end

  test "email should not be too long" do
    @user.email = "a"*101
    assert_not @user.valid?
  end

  test "email validation should accept valid addresses" do
  	valid_addresses = %w[user@example.com USER@foo.COM A_usr@foo.bar.org]
  	valid_addresses.each do |address|
  	  @user.email = address
  	  assert @user.valid?
  	end
  end

  test "email validation should reject invalid addresses" do
  	invalid_addresses = %w[user@example,com USER@foo@COM A_usrfoo.bar.org]
  	invalid_addresses.each do |address|
  	  @user.email = address
  	  assert_not @user.valid?
  	end
  end

  test "password should be present" do
    @user.password = ''
    @user.password_confirmation = ''
    assert_not @user.valid?
  end

  test "password should not be too short" do
    @user.password = "a"*5
    @user.password_confirmation = "a"*5
    assert_not @user.valid?
  end

end
